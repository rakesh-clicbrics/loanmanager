'use strict';
var app = angular.module('app', ['oc.lazyLoad','ui.router','ipCookie']);
app.config(function($stateProvider, $locationProvider, $urlRouterProvider, $httpProvider){
	$httpProvider.defaults.headers.common = {};
	$httpProvider.defaults.headers.post = {};
	$httpProvider.defaults.headers.put = {};
	$httpProvider.defaults.headers.patch = {};
	$locationProvider.hashPrefix('!');
	$urlRouterProvider.otherwise("/");
	
	$stateProvider
	.state('bank', {
		url: "/bank",
		templateUrl: "app/views/bank.html",
		controller: "bankController",
		resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('app/controllers/bankController.js'); // Resolve promise and load before view
            }]
        }
	}).state('bankproduct', {
		url: "/bankproduct?bank&name",
		templateUrl: "app/views/bankproduct.html",
		controller: "bankProductController",
		resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('app/controllers/bankProductController.js'); // Resolve promise and load before view
            }]
        }
	}).state('lead', {
		url: "/lead",
		templateUrl: "app/views/lead.html",
		controller: "leadController",
		resolve: {
            loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load('app/controllers/leadController.js'); // Resolve promise and load before view
            }]
        }
	});			
});

app.constant('appConstants', {
    API_URL: "http://35.184.210.208:3000/"
});
