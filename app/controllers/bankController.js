(function() {
	'use strict';
	angular.module('app').controller('bankController', ctrl);

	ctrl.$inject= ['$state','$scope', '$rootScope', 'apiServices'];

	function ctrl($state, $scope, $rootScope, apiServices){
		console.log('bank controller');
		$rootScope.pageTitle = 'Bank';
		getBankList();
		function getBankList(){
			$rootScope.loading = true;
			apiServices.getBankList()
			.then(function(data){
				console.log(data);
				$scope.banks = data||[];
				$rootScope.loading = false;
			},function(error){
				console.log(error);
				setAlert('error', 'Error in getting bank list.');
				$rootScope.loading = false;
			});
		}
		$scope.showBankModal = function(){
			if($scope.bankForm){
				$scope.bankForm.$submitted = false;
			}
			$scope.bank = {bankId:'bank_'+new Date().getTime(),name:''};
			$('#addNewBankModal').modal('show');
		}
		$scope.saveBank = function(){
			$scope.bankForm.$submitted = true;
			if($scope.bankForm.$invalid){
				return false;
			}
			$('#addNewBankModal').modal('hide');
			$rootScope.loading = true;			
			apiServices.addBank($scope.bank)
			.then(function(data){
				console.log(data);
				getBankList();
			},function(error){
				$rootScope.loading = false;
				console.log(error);
				setAlert('error', 'Error in adding bank.');
			})
		}
	}
})();
