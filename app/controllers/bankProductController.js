(function() {
	'use strict';
	angular.module('app').controller('bankProductController', ctrl);

	ctrl.$inject= ['$state','$scope', '$rootScope', 'apiServices'];

	function ctrl($state, $scope, $rootScope, apiServices){
		$rootScope.pageTitle = $state.params.name+' Bank Product';
		var bankId = $state.params.bank;
		getBankProductList();
		function getBankProductList(){
			$rootScope.loading = true;
			apiServices.getAllBankProducts()
			.then(function(data){
				var bp = data||[];
				$scope.bankProducts = bp.filter(function(b){return b.bank.substring(b.bank.indexOf("#") + 1)==bankId})
				$rootScope.loading = false;
				console.log($scope.bankProducts);
			},function(error){
				$rootScope.loading = false;
				console.log(error);
				setAlert('error','Error in getting bank product list.');
			});
		}
		$scope.bankSalesType = ['DSA','DST'];
		$scope.showBankProductModal = function(){
			if($scope.bankForm){
				$scope.bankForm.$submitted = false;
			}
			$scope.bankProduct = {code:'code_'+new Date().getTime(),name:'',bankSalesType:'',bank:'resource:com.clicbrics.mortgage.Bank#'+bankId,commission:''};
			$('#addNewBankProductModal').modal('show');
		}
		$scope.saveBankProduct = function(){
			$scope.bankForm.$submitted = true;
			if($scope.bankForm.$invalid){
				return false;
			}
			$('#addNewBankProductModal').modal('hide');
			$rootScope.loading = true;			
			apiServices.addBankProduct($scope.bankProduct)
			.then(function(data){
				console.log(data);
				getBankProductList();
			},function(error){
				$rootScope.loading = false;
				console.log(error);
				setAlert('error', 'Error in adding bank product.');
			})
		}
	}
})();
