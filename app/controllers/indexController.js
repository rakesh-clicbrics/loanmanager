(function() {
	'use strict';
	angular.module('app').controller('indexController', ctrl);

	ctrl.$inject= ['$state','$scope', '$rootScope'];

	function ctrl($state, $scope, $rootScope){
		console.log('index controller');
	}
})();
