(function() {
	'use strict';
	angular.module('app').controller('leadController', ctrl);

	ctrl.$inject= ['$state','$scope', '$rootScope', 'apiServices'];

	function ctrl($state, $scope, $rootScope, apiServices){
		$rootScope.pageTitle = 'Lead';
		$scope.showLeadModal = function(){
			if($scope.leadForm){
				$scope.leadForm.$submitted = false;
			}
			$scope.lead = {name:'',phone:'',email:''};
			$scope.loan = {loanId:'loan_'+new Date().getTime(),approvedAmount:0,receivedAmount:0,time: new Date().getTime(),state:'INIT'};
			$scope.transaction = {message:'',addedBy:'Loan Manager1',addedById:'1'};
			$('#addNewLeadModal').modal('show');
		}
		$scope.saveLead = function(){
			$scope.leadForm.$submitted = true;
			if($scope.leadForm.$invalid){
				return false;
			}
			$('#addNewLeadModal').modal('hide');
			$rootScope.loading = true;			
			apiServices.addLead($scope.lead)
			.then(function(data){
				$scope.loan.lead = 'resource:com.clicbrics.mortgage.Lead#'+$scope.lead.phone;
				apiServices.addLoan($scope.loan)
				.then(function(data){
					$scope.transaction.loan = 'resource:com.clicbrics.mortgage.Loan#'+$scope.loan.loanId;
					apiServices.addLoanRequirementTransaction($scope.transaction)
					.then(function(data){
						$rootScope.loading = false;
						setAlert('success','Lead added successfully.');
					},function(error){
						$rootScope.loading = false;
						console.log(error);
						setAlert('error','Error in adding lead.');
					});
				},function(error){
					$rootScope.loading = false;
					console.log(error);
					setAlert('error','Error in adding lead.');
				});
			},function(error){
				$rootScope.loading = false;
				console.log(error);
				setAlert('error','Error in adding lead.');
			});
			console.log($scope.lead,$scope.loan,$scope.transaction);
		}
	}
})();
