'use strict';
angular
.module('app')
.service('apiServices', ['$q','$http','$state','ipCookie','appConstants', function salesApiServices($q, $http, $state, ipCookie,appConstants) {
	// AngularJS will instantiate a singleton by calling "new" on this function
	var service = {
		'authenticated': null,
		'request': function(args) {
			// Let's retrieve the token from the cookie, if available
			if(args.token){
				var authToken = 'Bearer ' + args.token;
			}else if(ipCookie('token')){
				var authToken = 'Bearer ' + ipCookie('token').substr(1);
			}
			var userToken = ipCookie('token')?ipCookie('token').substr(1):'';
			// Continue
			var API_URL = appConstants.API_URL
			params = args.params || {}
			args = args || {};
			var deferred = $q.defer(),
			url = API_URL + args.url,
			method = args.method || "GET",
			params = params,
			data = args.data || {};
			$http({
				url: url,
				withCredentials: this.use_session,
				method: method.toUpperCase(),
				headers: {'Authorization' : authToken, 'content-type': 'application/json', 'access-control-allow-origin': '*', 'Access-Control-Allow-Credentials': true, 'agentId':ipCookie('agentId'), 'token':userToken},
				params: params,
				data: data
			})
			.success(angular.bind(this,function(data, status, headers, config) {
				deferred.resolve(data, status);
			}))
			.error(angular.bind(this,function(data, status, headers, config) {
				console.log("error syncing with: " + url);
			// Set request status
			if(data){
				data.status = status;
			}
			if(status == 0){
				if(data == ""){
					data = {};
					data['status'] = 0;
					data['non_field_errors'] = ["Could not connect. Please try again."];
				}
					// or if the data is null, then there was a timeout.
					if(data == null){
						// Inject a non field error alerting the user
						// that there's been a timeout error.
						data = {};
						data['status'] = 0;
						data['non_field_errors'] = ["Server timed out. Please try again."];
					}
				}
				deferred.reject(data, status, headers, config);
			}));
			return deferred.promise;
		},'getBankList': function(){
			return this.request({
				'method': "GET",
				'url': "api/Bank"
			})
		},'addBank': function(bank){
			return this.request({
				'method': "POST",
				'url': "api/Bank",
				'data': bank
			})
		},'getAllBankProducts': function(){
			return this.request({
				'method': "GET",
				'url': "api/BankProduct"
			})
		},'addBankProduct': function(bankProduct){
			return this.request({
				'method': "POST",
				'url': "api/BankProduct",
				'data': bankProduct
			})
		},'addLead': function(lead){
			return this.request({
				'method': "POST",
				'url': "api/Lead",
				'data': lead
			})
		},'addLoan': function(loan){
			return this.request({
				'method': "POST",
				'url': "api/Loan",
				'data': loan
			})
		},'addLoanRequirementTransaction': function(args){
			return this.request({
				'method': "POST",
				'url': "api/LoanRequirementTransaction",
				'data': args
			})
		}
	}
	return service;
}]);
