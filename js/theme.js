$(document).ready(function(){
	
	//Left menu accordion
	var menuItem = $(".left-menu > .menu-list > .menu-items > a");
	var subMenuList = $(".left-menu > .menu-list .sub-menu-list");
	
	menuItem.click(function(e){
		if($(this).next(".sub-menu-list").length){
			e.preventDefault();
			subMenuList.not($(this).next(".sub-menu-list")).stop().slideUp();
			$(this).next(".sub-menu-list").stop().slideToggle();
		}
	});
	
	//Left menu search
	
	var menuItem = $(".left-menu > .menu-list > .menu-items a");
	var menuSearch = $(".menu-search-box > input[type='text']");

	menuSearch.keyup(function(){		
		searchTerm = $(this).val().toLowerCase();
			if(searchTerm.length != 0){
					menuItem.parents(".menu-items").hide();
					menuItem.parents(".sub-menu-list").hide();
					menuItem.each(function(i){
							if($(this).text().toLowerCase().indexOf(searchTerm) >= 0 ){
								if($(this).parents(".sub-menu-list").length){
									$(this).parents(".sub-menu-list").show();
									$(this).parents(".sub-menu-list").parent(".menu-items").show();
								}else{
									$(this).parent(".menu-items").show();
								}	
							}
					});
				}else{
					menuItem.parents(".menu-items").show();
					menuItem.parents(".sub-menu-list").hide();
				}
	});

	//Mobile menu show hide
	var sidebar_toggle = $(".top-nav-bar .navbar-toggle");
	var wrapper = $(".wrapper");

	sidebar_toggle.click(function(){
		wrapper.toggleClass("show-sidebar");
	});
	
	//Tooltip
	jQuery("[data-toggle='tooltip']").tooltip();
	
	//Popover
	 $('[data-toggle="popover"]').popover();
	 
});

//alert
function setAlert(type, msg){
	type = type=='error'?'danger':type;
	$('.alert').hide();
	var alert = "<div class='alert themealert alert-"+type+" alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+msg+"</div>";
	$('body').append(alert);
	var lastALert = $('div.alert.themealert');
	setTimeout(function(){$(lastALert).remove()},3000);
}